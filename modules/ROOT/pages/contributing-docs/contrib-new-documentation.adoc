include::ROOT:partial$attributes.adoc[]

= Contributing a new documentation sites

[abstract]
____
This section describes how to add a complete new piece of documentation that covers a new area in its entirety. This spans several pages and is usually associated with the creation of a new, dedicated repository. A local working environment is best suited for this. But it is also possible to use the GitLab web IDE.
____
[NOTE]
====
The following text is slightly outdated, does not take into account the GitLab repositories and still needs to be adjusted. 
====
This section describes how to create and publish new documentation to the docs.fedoraproject.org website.
Before you start following this procedure, review all the requirements listed in xref:contributing-docs/index.adoc#prerequisites[Prerequisites].

[IMPORTANT]
====
Before creating a new documentation site, consult the {MAILING-LIST}[Discussion forum] first.
This is to make sure we can publish your work and you do not waste your time.
====

. Clone the template repository:
  link:++https://pagure.io/fedora-docs/template++[]

. Create a new repository for the new documentation set, or ask someone to create one for you.
  You can host this repository anywhere but it is recommended to host it on link:++https://pagure.io++[`pagure.io`] where you can reuse Fedora groups to control write access to the repository.
  Depending on the topic, it may also be best to host it under link:++https://pagure.io/projects/fedora-docs/%2A++[`fedora-docs`].

. Clone the newly created repository for your content set.

. Copy the contents of the template repository (without the [filename]`.git` directory) into the newly created repository or import the commit history from the template repository.

. In the new repository, edit the [filename]`antora.yml` configuration file in the repository root.
  The file contains comments that point out which parts you need to change.
  At a minimum, always change the `name` and `title`.

. Additionally, edit the [filename]`site.yml` configuration file.
  Note that this file is only used when building a local preview of your content set - on the website it is overridden by the site-wide `site.yml` configuration.
  The only directives you need to edit in this file is the `title` and `start_page`.

. At this point, when the initial configuration is finished and the repository is configured with the correct name and other required directives, push these changes to the newly created repository (or make a pull request if you can not push directly).
  This set of changes will be required for any other contributions, so make sure they are in as early as possible.

. Fork the new repository, so you are not pushing updates directly into it.

. Start adding the actual ASCIIDoc content.
  While writing, make sure your new source files are included in the [filename]`nav.adoc` configuration file of the module you are using
  ([filename]`./modules/ROOT/` by default, the location will change based on what you configured in [filename]`antora.yml` earlier).
  Also make sure to use xref:contributing-docs/tools-localenv-preview.adoc[local preview] often to check your markup.

. Once you finish, commit your changes and push them to your fork.

. Use Pagure to make a pull request from your fork to the main repository's `master` branch.

. Someone will see your pull request and either merge it, or provide feedback if there is something you should change.
  Work with the people commenting to make sure your contributions are up to standards.

. Your new content will need to be published for the first time, and at the moment this does not happen automatically.
  Send an e-mail to the {MAILING-LIST}[docs mailing list] asking for your content to be published.

[NOTE]
====
If nobody reacts to your Pull Request after 5 days, xref:ROOT:index.adoc#find-docs[get in touch with the Docs Team].
We might have missed the email for your Pull Request.
====
